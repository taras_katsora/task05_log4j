package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        log.trace("This is trace message");
        log.debug("This is debug message");
        log.info("This is info message");
        log.warn("This is warn message");
        log.error("This is error message");
        log.fatal("This is fatal message");
    }
}