package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

    public static final String ACCOUNT_SID = "AC506085706c92d1c5e5974e8e1e7bd21f";
    public static final String AUTH_TOKEN = "82fb86bd3b6b68af61f0050567c87fb1";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380984984489"),
                        new PhoneNumber("+17866299429"), str)
                .create();
    }
}